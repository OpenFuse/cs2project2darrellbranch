package edu.westga.cs1302.project2.model.manager;

import java.util.Random;

import edu.westga.cs1302.project2.model.Butterfly;
import edu.westga.cs1302.project2.model.Cloud;
import edu.westga.cs1302.project2.model.Dragonfly;
import edu.westga.cs1302.project2.model.Flying;
import edu.westga.cs1302.project2.model.Sky;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

/**
 * The Class SkyManager.
 * 
 * @author CS1302
 * @version Fall 2018
 */
public class SkyManager extends Pane {

	private Sky sky;
	
	/**
	 * Instantiates a new sky.
	 * 
	 * @precondition none
	 * @postcondition getSky() != null
	 */
	public SkyManager() {
		this.sky = new Sky();
		this.getChildren().add(this.sky);
	}

	/**
	 * Gets the sky.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the sky
	 */
	public Sky getSky() {
		return this.sky;
	}
	
	/**
	 * Sets the up bugs and clouds.
	 * 
	 * @precondition nInsects >= 0
	 * @postcondition getSky().getInsects().getSize() == nInsects
	 */
	public void setupBugsAndClouds() {
		this.generateInsects();
		this.generateClouds();
	}
	
	/**
	 * Generate insects.
	 */
	private void generateInsects() {
		Random random = new Random();
		Color[] allColors = {Color.ORANGE, Color.PINK, Color.BROWN, Color.CYAN};
		
		for (int i = 0; i < 8; i++) {
			int size = random.nextInt(50) + 50;
			int posX = random.nextInt(Sky.WIDTH - size);
			int posY = random.nextInt(Sky.HEIGHT - size - 90) + 90;
			Color color = allColors[random.nextInt(allColors.length)];
			int speed = random.nextInt(20) + 10;
			
			double randomNumber = random.nextDouble();
			
			if (randomNumber <= 0.75) {
				Butterfly butterfly = new Butterfly(posX, posY, size, color, speed);
				this.sky.addInsect(butterfly);
				this.getChildren().add(butterfly);
			} else {
				Dragonfly dragonfly = new Dragonfly(posX, posY, size, color, speed);
				this.sky.addInsect(dragonfly);
				this.getChildren().add(dragonfly);
			}
		}
	}
	
	/**
	 * Generate clouds.
	 */
	private void generateClouds() {
		Random random = new Random();
		
		for (int i = 0; i < 3; i++) {
			int posX = i * (Sky.WIDTH / 2);
			int posY = random.nextInt(15) + 5;
			
			Cloud cloud = new Cloud(posX, posY, 200, 75);
			this.sky.addCloud(cloud);
			this.getChildren().add(cloud);
		}
		
	}
	
	/**
	 * Move all flying objects in the sky
	 *  
	 * @precondition none
	 * @postcondition none
	 */
	public void update() {
		
		for (Flying flyingObject: this.sky) {
			flyingObject.fly();
		}
		
	}
}
