package edu.westga.cs1302.project2.model;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * The Class Dragonfly.
 * 
 * @author Darrell Branch
 */
public class Dragonfly extends WingedInsect {
	
	private Color color;
	
	/**
	 * Instantiates a new dragonfly.
	 *
	 * @param x the x
	 * @param y the y
	 * @param size the size
	 * @param color the color
	 * @param speed the speed
	 */
	public Dragonfly(double x, double y, double size, Color color, int speed) {
		super(x, y, size, size, speed);
		this.color = color;
		this.draw();
	}
	
	/**
	 * Gets the color.
	 *
	 * @precondition none
	 * @postcondition none
	 * @return the color
	 */
	public Color getColor() {
		return this.color;
	}
	
	@Override
	public void draw() {
		GraphicsContext gc = this.getGraphicsContext2D();
		//Draw Body
		gc.setFill(Color.BLACK);
		gc.fillOval(0, this.getHeight() / 2, this.getWidth(), this.getHeight() / 10);
		
		if (this.getDirection() == Direction.LEFT) {
			gc.fillOval(0, this.getHeight() / 2.5, this.getWidth() / 2, this.getHeight() / 4);
		} else {
			gc.fillOval(this.getWidth() - this.getWidth() / 2, this.getHeight() / 2.5, this.getWidth() / 2, this.getHeight() / 4);
		}
		//Draw Wings
		gc.setFill(this.color);
		if (this.getDirection() == Direction.LEFT) {
			gc.fillPolygon(new double[]{0, this.getWidth() / 4, this.getWidth() / 8}, 
					new double[] {0, 0, this.getHeight() / 2 + 5}, 3);
	
			gc.fillPolygon(new double[]{this.getWidth() / 4, this.getWidth() / 2, this.getWidth() / 4}, 
					new double[] {0, 0, this.getHeight() / 2}, 3);
		} else {
			gc.fillPolygon(new double[]{this.getWidth() / 2, this.getWidth() / 2 + this.getWidth() / 4, this.getWidth() / 2 + this.getWidth() / 8}, 
					new double[] {0, 0, this.getHeight() / 2}, 3);
			
			gc.fillPolygon(new double[]{this.getWidth() / 2 + this.getWidth() / 4, this.getWidth(), this.getWidth() - this.getWidth() / 8}, 
					new double[] {0, 0, this.getHeight() / 2}, 3);
		}
		//Draw Eye
		gc.setFill(Color.WHITE);
		if (this.getDirection() == Direction.LEFT) {
			gc.fillOval(0, this.getHeight() / 2, 6, 6);
		} else {
			gc.fillOval(this.getWidth() - 7, this.getHeight() / 2, 6, 6);
		}
	}
	
	@Override
	public String toString() {
		return "Dragonfly width=" + this.getWidth() + " speed=" + this.getSpeed();
	}

}
