package edu.westga.cs1302.project2.model;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * The Class Butterfly.
 * 
 * @author Darrell Branch
 */
public class Butterfly extends WingedInsect {
	
	private Color color;
	private boolean wingsUp;
	
	/**
	 * Instantiates a new butterfly.
	 *
	 * @param x the x
	 * @param y the y
	 * @param size the size
	 * @param color the color
	 * @param speed the speed
	 */
	public Butterfly(double x, double y, double size, Color color, int speed) {
		super(x, y, size, size, speed);
		this.color = color;
		this.draw();
		this.wingsUp = true;
	}

	/**
	 * Gets the color.
	 *
	 * @precondition none
	 * @postcondition none
	 * @return the color
	 */
	public Color getColor() {
		return this.color;
	}
	
	@Override
	public void draw() {
		GraphicsContext gc = this.getGraphicsContext2D();
		
		//Draw Body
		gc.setFill(Color.GRAY);
		gc.fillOval(0, this.getHeight() / 2, this.getWidth(), this.getHeight() / 6);
		
		//Draw Wings
		if (this.wingsUp) {
			this.drawWingUp();
			this.wingsUp = false;
		} else {
			this.drawWingDown();
			this.wingsUp = true;
		}
		
		//Draw Antenna
		gc.setStroke(Color.GRAY);
		
		if (this.getDirection() == Direction.LEFT) {
			gc.strokeLine(0, 30, this.getWidth() / 6, this.getHeight() / 2 + 2);
		} else {
			gc.strokeLine(this.getWidth(), 30, this.getWidth() - this.getWidth() / 6, this.getHeight() / 2 + 2);
		}
	}
	
	/**
	 * Draw wing up.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public void drawWingUp() {
		GraphicsContext gc = this.getGraphicsContext2D();
		
		gc.setFill(this.color);
		gc.fillPolygon(new double[]{0, this.getWidth(), this.getWidth() / 2}, 
						new double[] {0, 0, this.getHeight() / 2 + 5}, 3);
	}
	
	/**
	 * Draw wing down.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public void drawWingDown() {
		GraphicsContext gc = this.getGraphicsContext2D();
		
		gc.setFill(this.color);
		gc.fillPolygon(new double[]{0, this.getWidth(), this.getWidth() / 2}, 
						new double[] {this.getHeight(), this.getHeight(), this.getHeight() / 2}, 3);
	}
	
	@Override
	public void fly() {
		super.fly();
		
		this.clear();
		this.draw();
		
		if (this.wingsUp) {
			this.setLayoutY(this.getLayoutY() - 2);
		} else {
			this.setLayoutY(this.getLayoutY() + 2);
		}	
	}

	@Override
	public String toString() {
		return "Butterfly width=" + this.getWidth() + " color=" + this.color + " speed=" + this.getSpeed();
	}

}
