package edu.westga.cs1302.project2.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 * The Class Sky.
 * 
 * @author CS1302
 * @version Fall 2018
 */
public class Sky extends Rectangle implements Iterable<Flying> {
	
	public static final int WIDTH = 750;
	public static final int HEIGHT = 450;
	
	private List<WingedInsect> insectList;
	private ArrayList<Cloud> clouds;
	private ArrayList<Flying> allFlyingObjects;
	
	
	/**
	 * Instantiates a new sky.
	 * 
	 * @precondition none
	 * @postcondition insects.sze() == 0 && clouds.size() == 0
	 */
	public Sky() {
		super(0, 0, Sky.WIDTH, Sky.HEIGHT);
		this.setFill(Color.CORNFLOWERBLUE);
		this.insectList = new ArrayList<WingedInsect>();
		this.clouds = new ArrayList<Cloud>();
		this.allFlyingObjects = new ArrayList<Flying>();
	}
	
	/**
	 * Gets the insects.
	 *
	 * @return the insects
	 */
	public List<WingedInsect> getInsects() {
		return this.insectList;
	}
	
	/**
	 * Adds the.
	 *
	 * @precondition insect != null
	 * @postcondition getInsects() == getInsects()prev + 1;
	 * @param insect the insect
	 */
	public void addInsect(WingedInsect insect) {
		if (insect == null) {
			throw new IllegalArgumentException("The insect cannot be null.");
		}
		
		this.insectList.add(insect);
		this.allFlyingObjects.add(insect);
	}
	
	/**
	 * Adds the cloud
	 *
	 * @precondition cloud != null
	 * @postcondition getClouds() == getClouds()prev + 1;
	 * @param cloud the cloud
	 */
	public void addCloud(Cloud cloud) {
		if (cloud == null) {
			throw new IllegalArgumentException("The cloud cannot be null.");
		}
		
		this.clouds.add(cloud);
		this.allFlyingObjects.add(cloud);
		
	}
	
	@Override
	public Iterator<Flying> iterator() {
		return this.allFlyingObjects.iterator();
	}
}
