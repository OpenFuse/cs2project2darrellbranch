package edu.westga.cs1302.project2.model;

/**
 * The Enum Direction.
 * 
 * @author Darrell Branch
 */
public enum Direction {
	
	/** The left. */
	LEFT, 
	/** The right. */
	RIGHT;

}
