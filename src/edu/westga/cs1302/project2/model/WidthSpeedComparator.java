package edu.westga.cs1302.project2.model;

import java.util.Comparator;

/**
 * The Class WidthSpeedComparator.
 * 
 * @author Darrell Branch
 */
public class WidthSpeedComparator implements Comparator<WingedInsect> {

	/**
	 * Compare insects by width, then speed as a fallback.
	 * 
	 * @precondition none;
	 * @postcondition none;
	 * @param insectOne the first insect to compare
	 * @param insectTwo the second insect to compare
	 * @return the comparison
	 */
	public int compare(WingedInsect insectOne, WingedInsect insectTwo) {
		Double widthOne = insectOne.getWidth();
		Double widthTwo = insectTwo.getWidth();
		Integer speedOne = insectOne.getSpeed();
		Integer speedTwo = insectTwo.getSpeed();
		
		if (widthOne.equals(widthTwo)) {
			return speedOne.compareTo(speedTwo);
		} else {
			return widthOne.compareTo(widthTwo);
		}
	}
	
}
