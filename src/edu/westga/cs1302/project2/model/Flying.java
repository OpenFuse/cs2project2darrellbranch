package edu.westga.cs1302.project2.model;

/**
 * The Interface Flying.
 * 
 * @author Darrell Branch
 */
public interface Flying {

	/**
	 * Fly.
	 */
	void fly();
	
}
