package edu.westga.cs1302.project2.model;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * The Class Cloud.
 * 
 * @author Darrell Branch
 */
public class Cloud extends Canvas implements Flying {

	/**
	 * Instantiates a new cloud.
	 *
	 * @param x the x
	 * @param y the y
	 * @param width the width
	 * @param height the height
	 */
	public Cloud(double x, double y, double width, double height) {
		super(width, height);
		this.setLayoutX(x);
		this.setLayoutY(y);
		this.draw();
	}
	
	/**
	 * Clears the canvas.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public void clear() {
		GraphicsContext gc = this.getGraphicsContext2D();
		gc.clearRect(0, 0, this.getWidth(), this.getHeight());
	}
	
	/**
	 * Draws a cloud.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public void draw() {
		GraphicsContext gc = this.getGraphicsContext2D();
		gc.setFill(Color.WHITE);
		gc.fillOval(0, 0, this.getWidth(), this.getHeight());
	}
	
	/**
	 * Fly.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public void fly() {
		double currentPosX = this.getLayoutX();
		this.setLayoutX(currentPosX + 15);
		
		//reset if out of sight
		if (this.getLayoutX() >  Sky.WIDTH) {
			this.setLayoutX(0 - this.getWidth());
		}
	}
}
