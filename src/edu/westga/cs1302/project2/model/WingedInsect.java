package edu.westga.cs1302.project2.model;

import java.util.Random;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;

/**
 * The Class WingedInsect.
 * 
 * @author Darrell Branch
 */
public abstract class WingedInsect extends Canvas implements Comparable<Canvas>, Flying {
	
	private Direction direction;
	private int speed;
	
	/**
	 * Instantiates a new winged insect.
	 * 
	 * @precondition speed > 0;
	 * @postcondition getSpeed = speed
	 * @param x the x
	 * @param y the y
	 * @param width the width
	 * @param height the height
	 */
	protected WingedInsect(double x, double y, double width, double height, int speed) {
		super(width, height);
		
		if (speed <= 0) {
			throw new IllegalArgumentException("Speed must be positive");
		}
		
		this.setLayoutX(x);
		this.setLayoutY(y);
		Direction[] values = Direction.values();
		Random random = new Random();
		this.direction = values[random.nextInt(values.length)];
		this.speed = speed;
	}
	
	/**
	 * Compare to.
	 * 
	 * @param otherInsect the other insect
	 * @return the int
	 */
	public int compareTo(Canvas otherInsect) {
		double otherInsectWidth = otherInsect.getWidth();
		
		if (this.getWidth() > otherInsectWidth) {
			return 1;
		} else if (this.getWidth() < otherInsectWidth) {
			return -1;
		} else {
			return 0;
		}
	}

	/**
	 * Gets the direction.
	 *
	 * @precondition none
	 * @postcondition none;
	 * @return the direction
	 */
	public Direction getDirection() {
		return this.direction;
	}
	
	/**
	 * Clears the canvas.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public void clear() {
		GraphicsContext gc = this.getGraphicsContext2D();
		gc.clearRect(0, 0, this.getWidth(), this.getHeight());
	}
	
	/**
	 * Draw.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public void draw() {
		this.draw();
	}

	/**
	 * Gets the speed.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the speed
	 */
	public int getSpeed() {
		return this.speed;
	}
	
	/**
	 * Fly.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public void fly() {
		if (this.direction == Direction.LEFT) {
			//move left
			double currentPosX = this.getLayoutX();
			this.setLayoutX(currentPosX - this.speed);
			
			//reset if out of sight
			if (this.getLayoutX() < 0) {
				this.setLayoutX(Sky.WIDTH);
			}
		} else {
			//move right
			double currentPosX = this.getLayoutX();
			this.setLayoutX(currentPosX + this.speed);
			
			//reset if out of sight
			if (this.getLayoutX() >  Sky.WIDTH) {
				this.setLayoutX(0);
			}
		}
	}

	
}
