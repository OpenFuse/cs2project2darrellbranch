package edu.westga.cs1302.project2.view.resources;

/**
 * The Class UI defines output strings that are displayed for the user to see.
 * 
 * @author CS1302
 * @version Fall 2018
 */
public final class UI {

	/**
	 * Defines string messages for exception messages for sky animation.
	 */
	public static final class ExceptionMessages {
		public static final String NULL_SKY = "sky cannot be null.";
	}

}
