package edu.westga.cs1302.project2.view.output;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import edu.westga.cs1302.project2.model.Butterfly;
import edu.westga.cs1302.project2.model.Dragonfly;
import edu.westga.cs1302.project2.model.Sky;
import edu.westga.cs1302.project2.model.WidthSpeedComparator;
import edu.westga.cs1302.project2.model.WingedInsect;
import edu.westga.cs1302.project2.view.resources.UI;

/**
 * OutputFormatter builds summaries and reports for Skys.
 *
 * @author CS1302
 * @version Fall 2018
 */
public class OutputFormatter {
	
	/**
	 * Initializes the OutputFormatter
	 */
	public OutputFormatter() {
	}
	
	/**
	 * Creates a report by analyzing a given sky.
	 * 
	 * @precondition 	sky != null
	 * @postcondition 	none
	 *
	 * @param sky 		sky to analyze
	 * @return 			String with the given report
	 */
	public String buildReport(Sky sky) {
		if (sky == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_SKY);
		}
		List<WingedInsect> insects = sky.getInsects();
		Collections.sort(insects, new WidthSpeedComparator());
		int numberButterflies = 0;
		int numberDragonflies = 0;
		for (int i = 0; i < insects.size(); i++) {
			if (insects.get(i).getClass() == Butterfly.class) {
				numberButterflies++;
			} else {
				numberDragonflies++;
			}
		}
		String summary = "Sky Report:";
		summary += System.lineSeparator();
		summary += "Number butterflies: " +  numberButterflies + System.lineSeparator();
		summary += "Number dragonflies: " + numberDragonflies + System.lineSeparator();
		summary += System.lineSeparator();
		summary += "Smallest butterfly: " + this.getSmallestButterfly(sky) + System.lineSeparator();
		summary += "Largest butterfly: " + this.getLargestButterfly(sky) + System.lineSeparator();
		summary += "Smallest dragonfly: " + this.getSmallestDragonfly(sky) + System.lineSeparator();
		summary += "Largest dragonfly: " + this.getLargestDragonfly(sky) + System.lineSeparator();
		summary += System.lineSeparator();
		summary += "Insects in sorted order: " + System.lineSeparator();
		
		for (int i = 0; i < insects.size(); i++) {
			summary += insects.get(i).toString() + System.lineSeparator();
		}
		
		return summary;
	}
	
	/**
	 * Gets the butterflies.
	 *
	 * @param sky the sky
	 * @return the butterflies
	 */
	public ArrayList<Butterfly> getButterflies(Sky sky) {
		List<WingedInsect> insects = sky.getInsects();
		ArrayList<Butterfly> butterflies = new ArrayList<Butterfly>();
		
		for (WingedInsect insect: insects) {
			if (insect instanceof Butterfly) {
				butterflies.add((Butterfly) insect);
			}
		}
		
		Collections.sort(butterflies, new WidthSpeedComparator());
		return butterflies;
	}
	
	/**
	 * Gets the dragonflies.
	 *
	 * @param sky the sky
	 * @return the dragonflies
	 */
	public ArrayList<Dragonfly> getDragonflies(Sky sky) {
		List<WingedInsect> insects = sky.getInsects();
		ArrayList<Dragonfly> dragonflies = new ArrayList<Dragonfly>();
		
		for (WingedInsect insect: insects) {
			if (insect instanceof Dragonfly) {
				dragonflies.add((Dragonfly) insect);
			}
		}
		
		Collections.sort(dragonflies, new WidthSpeedComparator());
		return dragonflies;
	}
	
	/**
	 * Gets the smallest butterfly.
	 *
	 * @param sky the sky
	 * @return the smallest butterfly
	 */
	public Butterfly getSmallestButterfly(Sky sky) {
		ArrayList<Butterfly> butterflies = this.getButterflies(sky);
		Butterfly smallestButterfly = butterflies.get(0);
		return smallestButterfly;
	}
	
	/**
	 * Gets the largest butterfly.
	 *
	 * @param sky the sky
	 * @return the largest butterfly
	 */
	public Butterfly getLargestButterfly(Sky sky) {
		ArrayList<Butterfly> butterflies = this.getButterflies(sky);
		Butterfly largestButterfly = butterflies.get(butterflies.size() - 1);
		return largestButterfly;
	}
	
	/**
	 * Gets the smallest dragonfly.
	 *
	 * @param sky the sky
	 * @return the smallest dragonfly
	 */
	public Dragonfly getSmallestDragonfly(Sky sky) {
		ArrayList<Dragonfly> dragonflies = this.getDragonflies(sky);
		Dragonfly smallestDragonfly = dragonflies.get(0);
		return smallestDragonfly;
	}
	
	/**
	 * Gets the largest dragonfly.
	 *
	 * @param sky the sky
	 * @return the largest dragonfly
	 */
	public Dragonfly getLargestDragonfly(Sky sky) {
		ArrayList<Dragonfly> dragonflies = this.getDragonflies(sky);
		Dragonfly largestDragonfly = dragonflies.get(dragonflies.size() - 1);
		return largestDragonfly;
	}
}
